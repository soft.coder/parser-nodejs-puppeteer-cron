class AuthorizeSelectors
{
    constructor(showLoginModalBtn, loginModal, loginBtn) {
        this.showLoginModalBtn = showLoginModalBtn;
        this.loginModal = loginModal;
        this.loginBtn = loginBtn;
    }
}
module.exports = { AuthorizeSelectors };
