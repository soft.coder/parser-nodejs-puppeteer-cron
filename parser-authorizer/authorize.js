
class Authorize
{
    /**
     *
     * @param browser
     * @param startUrl
     * @param selectors AuthorizeSelectors
     */
    constructor(browser, startUrl, selectors) {
        this.browser = browser;
        this.startUrl = startUrl;
        this.selectors = selectors;
    }

    async authorize(login, password){
        const page = await this.browser.newPage();
        await page.goto(this.startUrl);
        const showLoginPopupBtn = await page.$(this.selectors.showLoginModalBtn);
        if(showLoginPopupBtn == null) {
            return true;
        }
        await showLoginPopupBtn.click();
        await page.waitForSelector(this.selectors.loginModal);

        await this.enterCredentials(page, login, password);

        const loginBtn = await page.$(this.selectors.loginBtn);
        await loginBtn.click();
        await page.waitForSelector(this.selectors.showLoginModalBtn, {hidden: true, timeout: 1000 * 60 * 5 /* 5min */}); // wait reaCaptcha
    }
    async enterCredentials(page, login, password) {
        throw new Error('Not implemented');
    }
}
module.exports = { Authorize };
