const process = require('process');
const fs = require('fs');
const puppeteer = require('puppeteer');
const http = require('http');
// const config = require('./config');
const { AuthorizeAvito } = require('./authorize-avito');
const { AuthorizeYoula } = require('./authorize-youla');
const { Aes, Request } = require('parser-avito-and-youla-common');

process.on('unhandledRejection', error => {
    console.log(`unhandledRejection constructor: '${error && error.constructor}',` +
        `message: '${error && error.message}', ` +
        `fileName: '${error && error.fileName}', ` +
        `lineNumber: '${error && error.lineNumber}'`);
    process.exit();
});

async function main() {

    const browser = await puppeteer.launch({
        headless: false,
        defaultViewport: {
            width: 1920,
            height: 1080
        },
        userDataDir: './chromeUserDataDir',
        args: ['--no-sandbox', '--disable-setuid-sandbox']});

    let avito, youla;
    try {
        const config = await getCredentials();
        avito = new AuthorizeAvito(browser);
        await avito.authorize(config.avito.login, config.avito.password);
        youla = new AuthorizeYoula(browser);
        await youla.authorize(config.youla.phone);
        const page = await browser.newPage();
        const data = await page._client.send('Network.getAllCookies');
        const request = new Request('<ip-address>', 8080);
        await request.postEncrypted('/user-data', data);
    } catch(error) {
        throw error;
    }

    await browser.close();
}

main().finally(() => {
    process.exit();
});

async function getCredentials() {
    const configPath = './config.bin';
    const aes = new Aes();
    aes.importKeyAndIv("<key>");
    function readFromFile() {
        const configEncrypted = fs.readFileSync(configPath);
        return JSON.parse(aes.decrypt(configEncrypted.toString()));
    }
    function enterCredentials() {
        return new Promise(resolve => {
            process.stdin.addListener("data", readLine);
            let i = 0;
            const config = {
                avito: {},
                youla: {}
            };
            let isSave;
            const form = [
                { question: 'Введите номер телефона Юлы:', setValue(val) { config.youla.phone = val }},
                { question: 'Введите логин Avito:', setValue(val) { config.avito.login = val } },
                { question: 'Введите пароль Avito:', setValue(val) { config.avito.password = val } },
                { question: 'Сохранить введенные учетные данные [Y,N]:', setValue(val) { isSave = val.toLowerCase() === 'y'; } }
            ];
            console.log(form[i].question);
            function readLine(d) {
                // note:  d is an object, and when converted to a string it will
                // end with a linefeed.  so we (rather crudely) account for that
                // with toString() and then trim()
                const enterValue = d.toString().substring(0, d.toString().length - 1);
                form[i++].setValue(enterValue);
                if(i < form.length) {
                    console.log(form[i].question);
                } else {
                    process.stdin.removeListener("data", readLine);
                    if(isSave)
                        fs.writeFileSync(configPath, aes.encrypt(JSON.stringify(config)));
                    resolve(config);
                }
            }
        });
    }

    try {
        return readFromFile();
    } catch(fileError) {
        try {
            return await enterCredentials()
        } catch (enterError) {
            process.exit(1);
        }
    }
}
