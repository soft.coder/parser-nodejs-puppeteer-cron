const { Authorize } = require('./authorize');
const { AuthorizeSelectors } = require('./authorize-selectors');

class AuthorizeAvito extends Authorize
{
    constructor(browser) {
        const startUrl = 'https://www.avito.ru/rossiya/vakansii/stroitelstvo?cd=1&s=104&bt=1&q=%D0%B1%D1%80%D0%B8%D0%B3%D0%B0%D0%B4%D0%B0'
        let selectors = new AuthorizeSelectors(
            '[data-marker="header/login-button"]',
            '[data-marker="auth-app/overlay"]',
            '[data-marker="login-form/submit"]'
        );
        super(browser, startUrl,selectors );
    }

    async enterCredentials(page, login, password) {
        const loginInput = await page.$('[data-marker="login-form/login"]');
        await loginInput.focus();
        await loginInput.type(login, {delay: 100});
        const passwordInput = await page.$('[data-marker="login-form/password"]');
        await passwordInput.focus();
        await passwordInput.type(password, {delay: 100});
    }
}

module.exports = { AuthorizeAvito };
