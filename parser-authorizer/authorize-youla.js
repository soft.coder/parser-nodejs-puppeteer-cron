const { Authorize } = require('./authorize');
const { AuthorizeSelectors } = require('./authorize-selectors');

class AuthorizeYoula extends Authorize {
    constructor(browser) {
        const startUrl = 'https://youla.ru/all/rabota/stroitelstvo-remont?attributes[sort_field]=date_published&attributes[term_of_placement][from]=-1%20day&attributes[term_of_placement][to]=now&q=%22%D0%B1%D1%80%D0%B8%D0%B3%D0%B0%D0%B4%D0%B0%22'
        let selectors = new AuthorizeSelectors(
            '[data-test-action="LoginClick"]',
            '.auth > .box__wrapper',
            '.auth_group_button'
        );
        super(browser, startUrl,selectors);
    }
    async enterCredentials(page, login) {
        const byPhoneNumberBtn = await page.$('.auth_group_button');
        await byPhoneNumberBtn.click();
        const loginInput = await page.$('.auth .form_inline--tel .row div:nth-child(2) .form_control');
        await loginInput.focus();
        await loginInput.type(login, {delay: 100});
    }
}

module.exports = { AuthorizeYoula };
