const http = require('http');
const { Rsa } = require('./rsa-crypt');
const { Aes } = require('./aes');

class Request
{
    constructor(hostname = '127.0.0.1', port = 80) {
        this.hostname = hostname;
        this.port = port
    }
    send(path, data, method) {
        return new Promise((resolve, reject) => {
            const options = {
                hostname: this.hostname,
                port: this.port,
                path: path,
                method: method,
                headers: {
                    'Content-Type': 'text/plain',
                    'Content-Length': data.length
                }
            };

            const req = http.request(options, (res) => {
                if (res.statusCode === 200) {
                    resolve();
                    let body = [];
                    res.on('data', (chunk) => {
                        body.push(chunk);
                    }).on('end', () => {
                        body = Buffer.concat(body).toString();
                        //console.log(body);
                    });
                } else {
                    reject(new Error(`Receive response with invalid statusCode:${res.statusCode}, statusMessage:${res.statusMessage}`));
                }
            });

            req.on('error', (error) => {
                reject(new Error(`Error occurred when invoke request ${error} message: ${error.message} stack: '${error.stack}'`));
            });

            req.write(data);
            req.end()
        });

    }
    async postEncrypted(path, data) {
        try {
            const aes = new Aes();
            aes.generateKey();
            const exportedKeyIv = aes.exportKeyAndIv();
            const encryptedAesKeyIv = Rsa.encrypt(exportedKeyIv);
            await this.send(path, encryptedAesKeyIv, 'PUT');
            const encryptedData = aes.encrypt(JSON.stringify(data));
            await this.send(path, encryptedData, 'POST');
        } catch (error) {
            console.log(`Handshake server error, message: ${error.message}`);
        }
    }
}
module.exports = { Request };
