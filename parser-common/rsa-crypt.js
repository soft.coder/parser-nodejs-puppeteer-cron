const crypto = require('crypto');
const fs = require('fs');


class Rsa {
    static passphrase = '<passphrase>';
    static publicKeyPath = './public-key.pem';
    static privateKeyPath = './private-key.pem';
    static generateKeys() {
        crypto.generateKeyPair('rsa', {
            modulusLength: 4096,
            publicKeyEncoding: {
                type: 'spki',
                format: 'pem'
            },
            privateKeyEncoding: {
                type: 'pkcs8',
                format: 'pem',
                cipher: 'aes-256-cbc',
                passphrase: Rsa.passphrase
            }
        }, (err, publicKey, privateKey) => {
            fs.writeFileSync(Rsa.publicKeyPath, publicKey);
            fs.writeFileSync(Rsa.privateKeyPath, privateKey);
        })
    }

    static encrypt(data) {
        const publicKey = fs.readFileSync(Rsa.publicKeyPath, 'utf8');
        const buffer = Buffer.from(data, 'utf8');
        const encrypted = crypto.publicEncrypt(publicKey, buffer);
        return encrypted.toString('base64')
    }

    static decrypt(data) {
        const privateKey = fs.readFileSync(Rsa.privateKeyPath, 'utf8');
        const buffer = Buffer.from(data, 'base64');
        const decrypted = crypto.privateDecrypt(
            {
                key: privateKey.toString(),
                passphrase: Rsa.passphrase,
            },
            buffer,
        );
        return decrypted.toString('utf8')
    }
}
module.exports = { Rsa };


function test() {
    Rsa.generateKeys();
    let encrypted = Rsa.encrypt('hello world');
    console.log(encrypted);
    try {
        let decrypted = Rsa.decrypt(encrypted);
        console.log(decrypted);
    } catch (error) {
        console.log(error);
    }
}

