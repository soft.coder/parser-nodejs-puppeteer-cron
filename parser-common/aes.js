const crypto = require('crypto');

class Aes
{
    algorithm = 'aes-192-cbc';
    constructor() {
    }
    generateKey() {
        const password = '<password>';
        this.key = crypto.scryptSync(password, '<password>', 24);
        this.iv = crypto.randomBytes(16); // Buffer.alloc(16, 0); // Initialization vector.
        this.createCipher();
        this.createDeCipher();
    }
    createCipher() {
        this.cipher = crypto.createCipheriv(this.algorithm, this.key, this.iv);
    }
    createDeCipher() {
        this.decipher = crypto.createDecipheriv(this.algorithm, this.key, this.iv);
    }
    encrypt(text) {
        let encrypted = this.cipher.update(text, 'utf8', 'hex');
        encrypted += this.cipher.final('hex');
        return encrypted;
    }
    decrypt(text) {
        let decrypted = this.decipher.update(text, 'hex', 'utf8');
        decrypted += this.decipher.final('utf8');
        return decrypted;
    }
    exportKeyAndIv() {
        return `${this.key.toString('base64')} ${this.iv.toString('base64')}`;
    }
    importKeyAndIv(text) {
        const [key64, iv64] = text.split(' ');
        this.key = Buffer.from(key64, 'base64');
        this.iv = Buffer.from(iv64, 'base64');
        this.createCipher();
        this.createDeCipher();
    }
}
module.exports = { Aes };


function test() {
    function compareResults(text, decrypted) {
        if (text === decrypted) {
            console.log('success decrypt');
        } else {
            console.log('fail decrypt');
        }
    }

    try {
        let aes = new Aes();
        aes.generateKey();
        const text = 'Hello world';
        const encrypted = aes.encrypt(text);
        const decrypted = aes.decrypt(encrypted);
        compareResults(text, decrypted);
        const exportedKeyIv = aes.exportKeyAndIv();
        const aes2 = new Aes();
        aes2.importKeyAndIv(exportedKeyIv);
        const decrypted2 = aes2.decrypt(encrypted);
        compareResults(text, decrypted2);
    } catch (e) {
        console.log(e);
    }
}

