const {Aes} = require('../parser-avito-and-youla-common/aes');
const {Request} = require('../parser-avito-and-youla-common/request');
const {Rsa} = require('../parser-avito-and-youla-common/rsa-crypt');

module.exports = {Aes, Request, Rsa};


async function main(action) {
    console.log('start restarting parser avito and youla service');
    const password = '<password>';
    const data = {password, action};
    const request = new Request('<ip>', 8081);
    await request.postEncrypted('/service-manager', data);
    console.log('restarting parser avito and youla service completed')
}

main('restart')
    .catch(e => {
        console.log('failed to restart parser avito and youla service,' +
            ' error message: ' + e.message,
            ' stack: ' + e.stack);
    })
    .finally(() => {
        console.log('process terminating...');
    });
