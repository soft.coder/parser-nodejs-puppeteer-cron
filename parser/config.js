module.exports = {
    hostname: '<ip>',
    userDataPath: './user-data.json',
    avito: {
        startUrl: 'https://www.avito.ru/rossiya/vakansii/stroitelstvo?cd=1&s=104&bt=1&q=%D0%B1%D1%80%D0%B8%D0%B3%D0%B0%D0%B4%D0%B0'
    },
    youla: {
        startUrl: 'https://youla.ru/all/rabota/stroitelstvo-remont?attributes[sort_field]=date_published&attributes[term_of_placement][from]=-1%20day&attributes[term_of_placement][to]=now&q=%22%D0%B1%D1%80%D0%B8%D0%B3%D0%B0%D0%B4%D0%B0%22'
    },
    cron: {
        hour: 19,
    },
    browser: {
        headless: true
    },
    catalog: {
        screenshot: 'screenshot-logs',
        content: 'content-logs'
    }
};
