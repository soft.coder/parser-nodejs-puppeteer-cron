const {Parser} = require('./parser');
const {YoulaItem} = require('./parse-item');
const {YoulaItemCollection} = require('./parse-item-collection');
const {ItemSelectors} = require('./item-selectors');
const logger = require('./logger');
const config = require('./config');
const moment = require('moment');
const {formatError} = require('./utils');

class ParserYoula extends Parser
{
    constructor(browser, cookies) {
        const selectors = new ItemSelectors(
            'html > body > div > div > div > main >  div > div > div > div > section > div > div > div > div > h2',
            '[data-test-block="Description"] > dl > dd > p',
            '[data-test-component="UserCell"] > div > p > a',
            '[data-test-component="ProductStats"] [data-test-component="DescriptionList"] dd:last-child',
            '[data-test-component="ProductMap"] > dd > div > div > span',
            '[data-test-action="PhoneNumberClick"]',
            '[data-test-component="Modal"]',
            '[data-test-action="LoginClick"]'
        );
        const startUrl = config.youla.startUrl;
        super(browser, cookies, startUrl, selectors);
        this.geolocationModalSelector = '[data-test-component="GeolocationModal"]';
    }
    async start() {
        return await super.start();
    }
    async showGeolocationModalAndSwitchTabToCities() {
        let showModalBtnSelector = '[data-test-action="SelectGeolocationClick"]';
        await this.page.waitForSelector(showModalBtnSelector);
        const changeGeolocationShowModalBtn = await this.page.$(showModalBtnSelector);
        await this.page.waitFor(5 * 1000);
        await changeGeolocationShowModalBtn.click();
        await this.page.waitForSelector(this.geolocationModalSelector, {timeout: 10 * 60 * 1000});
        let cityTabSelector = `${this.geolocationModalSelector} div > div:first-child > div > li:last-child`;
        await this.page.waitForSelector(cityTabSelector);
        const cityTab = await this.page.$(cityTabSelector);
        await cityTab.click();
    }
    async getCities() {
        let citiesSelector = `${this.geolocationModalSelector} > div > div:nth-child(2) > div:last-child > div:last-child > div > div`;
        await this.page.waitForSelector(citiesSelector);
        return await this.page.$$(citiesSelector)
    }
    async getUrlsOfItems() {
        const urls = [];
        await this.showGeolocationModalAndSwitchTabToCities();
        let cities = await this.getCities();
        const citiesCount = cities.length;
        for(let i=0; i < citiesCount; i++) {
            await cities[i].click();
            let loadMoreSelector = '._paginator_next_button';
            await this.page.waitForNavigation({timeout: 10 * 60 * 1000});
            await this.page.waitForSelector('.product_section');
            const loadMore = await this.page.$(loadMoreSelector);
            let countItems = 0;
            if(loadMore != null) {
                do {
                    await loadMore.click();
                    try {
                        await this.page.waitForSelector(`${loadMoreSelector}.button--flat_disabled.button--disabled_link`, {delay: 1000});
                    } catch (error) {
                        continue;
                    }
                    break;
                } while (true);
                try {
                    let listItemsSelector = '.product_list .product_item > a[href^="/"]';
                    await this.page.waitForSelector(listItemsSelector, {timeout: 2 * 60 * 1000});
                    const itemEls = await this.page.$$(listItemsSelector);
                    for (const item of itemEls) {
                        try {
                            let itemSelector = '.product_item__title';
                            let itemJsHandle = await item.evaluateHandle(node => node);
                            await this.page.waitForFunction(itemNode => {
                                return itemNode.querySelectorAll('.product_item__title').length > 0;
                            }, {polling: "mutation"}, itemJsHandle);
                            let itemTitleEl = await item.$(itemSelector);
                            const title = await itemTitleEl.evaluate(node => node.textContent);
                            if (title.toLowerCase().includes('бригад')) {
                                urls.push(await item.evaluate(node => node.href));
                                countItems++;
                            }
                        } catch (e) {
                            logger.error(`Unexpected error occurred when try parse url of youla item, ${formatError(e)}`);
                        }
                    }
                } catch (e) {
                    logger.error(`Unexpected error occurred when try parse youla items, ${formatError(e)}`);
                }
            }
            if(i > 0 && i % 5 === 0) {
                const url = await this.page.url();
                await this.restartBrowser();
                await this.page.goto(url);
            }
            await this.showGeolocationModalAndSwitchTabToCities();
            cities = await this.getCities();
            if(cities[i]) {
                const cityName = await cities[i].evaluate(node => node.textContent);
                logger.info(`${i} of ${cities.length}: Get ${countItems} items of youla from ${cityName}`);
            }
        }
        return urls;

    }

    async parsePhoneNumber() {
        try {
            let phoneNumberSelector = '[data-test-component="ProductPhoneNumberModal"] > div > div > a';
            await this.page.waitForSelector(phoneNumberSelector);
            return await this.getTextContent(phoneNumberSelector);
        } catch (error) {
            const phoneDisabledForCurrentTime = await this.page.$('[data-test-component="ProductDisabledCallModal"]');
            const phoneDisabledElement = await this.page.$('[data-test-component="ProductPhoneNumberModal"] > div > div > p');
            if(phoneDisabledForCurrentTime != null) {
                logger.warn("Youla: phone disabled for current time url: " + this.page.url());
            } else if(phoneDisabledElement != null ) {
                const textContent = await phoneDisabledElement.evaluate(node => node.textContent.trim());
                if(textContent === 'Продавец запретил входящие звонки.') {
                    logger.warn("Youla: seller forbid income phone calls, url: " + this.page.url());
                } else throw error;
            } else throw error;
        }
    }
    parseRegionEl(text) {
        if(text && text.split(', ').length > 1) {
            const [region, city] = text.split(', ');
        return {region, city};
        } else {
            return {region: text, city: ''};
        }
    }
    createParseItem(title, description, name, date, region, city, phone, email, url) {
        return new YoulaItem(title, description, name, date, region, city, phone, email, url);
    }
    createParseItemCollection() {
        return new YoulaItemCollection();
    }
}

module.exports = {ParserYoula};
