const process = require('process');
const logger = require('./logger');
const {main} = require('./index');
const {formatError} = require('./utils');

process.on('unhandledRejection', error => {
    logger.error(`unhandledRejection: ${formatError(error)}`);
});

main();

