const fs = require('fs');
const process = require('process');
const { spawn } = require('child_process');

class ProcessManager
{
    static runServer() {
        ProcessManager.spawnSeparateProcess(process.argv[0], ['server-user-data.js'])
    }

    static spawnSeparateProcess(command, args) {
        const out = fs.openSync('./out.log', 'a');
        const err = fs.openSync('./out.log', 'a');
        const subprocess = spawn(command, args, {
            detached: true,
            stdio: [ 'ignore', out, err ]
        });
        subprocess.unref();
    }
}
module.exports = { ProcessManager };
