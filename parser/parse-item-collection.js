const {ParseItem} = require('./parse-item');
const {createObjectCsvStringifier} = require('csv-writer');

class ParseItemCollection
{
    constructor() {
        this.items = [];
    }
    push(item) {
        if(item instanceof ParseItem)
            this.items.push(item);
        else
            throw new TypeError("Must be ParseItem");
    }
    getLength() {
        return this.items.length;
    }
    toObject() {
        const result = [];
        for(const item of this.items) {
            result.push(item.toObject());
        }
        return result;
    }
    toJson() {
        return JSON.stringify(this.toObject());
    }
    toCsv() {
        const csvStringifier = createObjectCsvStringifier({
            header: [
                {id: 'title', title: 'Заголовок'},
                {id: 'description', title: 'Описание'},
                {id: 'date', title: 'Дата'},
                {id: 'region', title: 'Регион'},
                {id: 'city', title: 'Город'},
                {id: 'email', title: 'Почта'},
                {id: 'phone', title: 'Телефон'},
                {id: 'name', title: 'Имя'},
                {id: 'url', title: 'Ссылка'}
            ],
            fieldDelimiter: ';',
        });

        const header = csvStringifier.getHeaderString();
        const body = csvStringifier.stringifyRecords(this.toObject());
        return header + body;
    }

}
class AvitoItemCollection extends ParseItemCollection
{
    constructor() {
        super();
    }
}
class YoulaItemCollection extends ParseItemCollection
{
    constructor() {
        super();
    }
}

module.exports = {ParseItemCollection, AvitoItemCollection, YoulaItemCollection};
