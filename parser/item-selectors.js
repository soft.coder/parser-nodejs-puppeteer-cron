

class ItemSelectors {
    constructor(title, description, name, date, region, phoneNumberBtn, phoneNumberModel, showLoginModalBtn) {
        this.title = title;
        this.date = date;
        this.region = region;
        this.description = description;
        this.name = name;
        this.phoneNumberBtn = phoneNumberBtn;
        this.phoneNumberModal = phoneNumberModel;
        this.showLoginModalBtn = showLoginModalBtn;
    }
}
module.exports = {ItemSelectors};
