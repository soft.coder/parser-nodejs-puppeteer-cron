const process = require('process');
const fs = require('fs');
const logger = require('./logger');
const {Server} = require('./server');
const {formatError} = require('./utils');
const config = require('./config');

const port = 8080;


try {
    const server = new Server(port);
    server.run('/user-data', body => {
        const cookiesData = JSON.parse(body);
        fs.writeFileSync(config.userDataPath, JSON.stringify(cookiesData));
    }, () => process.exit());
} catch (error) {
    logger.error(`Unexpected error, when running server message: ${formatError(error)}`);
    process.exit(1);
}
