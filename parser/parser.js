const moment = require('moment');
const logger = require('./logger');
const fs = require('fs');
const config = require('./config');
const {NotImplementedAbstractMethodError, RequiredAuthorization} = require('./errors');
const {formatError, browserLauncher, killBrowser, timeout, restartBrowser} = require('./utils');

class Parser
{
    /**
     *
     * @param browser Browser
     * @param startUrl String
     * @param selectors ItemSelectors
     */
    constructor(browser, cookies, startUrl, selectors) {
        this.browser = browser;
        this.cookies = cookies;
        // for(const cookie of this.cookies.cookies) {
        //     cookie.url = 'https://www.avito.ru';
        // }
        this.startUrl = startUrl;
        this.selectors = selectors;
    }
    async start() {
        await this.retryPageHandler(async () => {
            this.page = await this.createPage();
            this.page.setDefaultTimeout(5 * 60 * 1000);
            // const data = await this.page._client.send('Network.getAllCookies');
            if (this.cookies) {
                await this.page.setCookie(...this.cookies.cookies);
            }
            await this.page.goto(this.startUrl, {waitUntil: 'domcontentloaded', timeout: 10 * 60 * 1000});
            const showLoginModalBtn = await this.page.$(this.selectors.showLoginModalBtn);
            if (showLoginModalBtn != null) {
                throw new RequiredAuthorization();
            }
            this.urlOfItems = await this.getUrlsOfItems();
        });
        const items = this.createParseItemCollection();
        logger.info(`start parsing ${this.urlOfItems.length} item`);
        let index = 0;
        let count = this.urlOfItems.length;
        for (const url of this.urlOfItems) {
            logger.info(`parse ${++index} of ${count} item: ${url}`);
            try {
                await this.retryPageHandler(async () =>  {
                    if(index % 5 === 0) {
                        await this.restartBrowser();
                    }
                    items.push(await this.parseItem(url));
                });
            } catch (e) {
                logger.error(`Unexpected error occurred when parse, url: ${url}, error message: ${e.message}, stack: ${e.stack}`);
            }
        }
        return items;
    }
    async parseItem(url) {
            await this.page.goto(url, {timeout: 10 * 60 * 1000});
            await this.page.waitFor(5 * 1000);
            const title = await this.getTextContent(this.selectors.title);
            logger.info(`title: ${title}`);
            const description = await this.getTextContent(this.selectors.description);
            logger.info(`description: ${description}`);
            const name = await this.getTextContent(this.selectors.name);
            logger.info(`name: ${name}`);
            const date = this.parseTime(await this.getTextContent(this.selectors.date));
            logger.info(`date: ${date}`);
            let {region, city} = this.parseRegionEl(await this.getTextContent(this.selectors.region));
            logger.info(`region: ${region}; city: ${city}`);
            const phone = await this.openModalAndParsePhoneNumber();
            logger.info(`phone: ${phone}`);
        return this.createParseItem(title, description, name, date, region, city, phone, undefined, url);
    }
    async getUrlsOfItems() {
        throw new NotImplementedAbstractMethodError;
    }
    async openModalAndParsePhoneNumber(deep = 0) {
        try {
            await this.page.waitForSelector(this.selectors.phoneNumberBtn);
            const phoneButton = await this.page.$(this.selectors.phoneNumberBtn);
            await this.page.waitFor(5 * 1000);
            await phoneButton.click();
            try {
                await this.page.waitForSelector(this.selectors.phoneNumberModal);
                return await this.parsePhoneNumber();
            } catch (e) {
                logger.warn(`Failed to parse phone number on  error: ${formatError(e)}`);
                await this.logCurrentState('failed-to-parse-phone');
                if(deep < 2) {
                    logger.warn(`Retry parse phone number attempt: ${deep + 1}`);
                    await this.restartBrowserAndLoadCurrentPage();
                    return await this.openModalAndParsePhoneNumber(deep + 1);
                }
            }
        } catch (e) {
            logger.warn(`Failed to found button that open phone number dialog on  error: ${formatError(e)}`);
            await this.logCurrentState('failed-to-open-phone-modal');
            if(deep < 2) {
                logger.warn(`Retry open phone number modal attempt: ${deep + 1}`);
                await this.restartBrowserAndLoadCurrentPage();
                return await this.openModalAndParsePhoneNumber(deep + 1);
            }
        }
    }
    async parsePhoneNumber() {
        throw new NotImplementedAbstractMethodError;
    }
    parseRegionEl(text) {
        throw new NotImplementedAbstractMethodError
    }
    parseTime(text) {
        if(text && text.split(' ').length > 2) {
            const time = text.split(' ')[2];
            if(text.toLowerCase().includes('сегодня')) {
                const today = moment().format('DD.MM.YYYY');
                return `${time}  ${today}`;
            } else if(text.toLowerCase().includes('вчера')) {
                const yesterday = moment().subtract(1, 'days').format('DD.MM.YYYY');
                return `${time} ${yesterday}`;
            } else return text;
        } else return text;
    }
    createParseItem(title, date, region, city, phone, email) {
        throw new NotImplementedAbstractMethodError;
    }
    createParseItemCollection() {
        throw new NotImplementedAbstractMethodError;
    }
    async getProperty(selector, propName, deep = 0) {
        try {
            await this.page.waitForSelector(selector, {timeout: 60 * 1000});
            let elementHandle = await this.page.$(selector);
            if (elementHandle != null) {
                return await elementHandle.evaluate((node, prop) => node[prop].trim(), propName);
            }
        } catch(error) {
            try {
                await this.logCurrentState('not-found-property');
                if(deep < 2) {
                    try {
                        logger.warn(`Not found attempt: '${deep + 1}' property: '${propName}' in '${selector}': ${formatError(error)}`);
                        await this.restartBrowserAndLoadCurrentPage();
                        return await this.getProperty(selector, propName, deep + 1);
                    } catch(restartError) {
                        logger.error(`Failed to restart get property error: ${formatError(restartError)}`);
                    }
                }
            } catch (e) {
                logger.warn(`Failed to make screenshot when not found property error: ${formatError(error)}`);
            }
        }
    }
    async getTextContent(selector) {
        return this.getProperty(selector, 'textContent');
    }
    async restartBrowserAndLoadCurrentPage() {
        const url = await this.page.url();
        await this.restartBrowser();
        await this.page.goto(url, {timeout: 10 * 60 * 1000});
        await this.page.waitFor(20 * 1000);
    }
    getNowDateTimeString() {
        return moment().format("HH-mm-ss DD-MM-YYYY");
    }
    async makeScreenshot(name) {
        let now = this.getNowDateTimeString();
        const fileName = `./${config.catalog.screenshot}/${name}-page-state-${now}.png`;
        await this.page.screenshot({path: fileName, fullPage: true});
    }
    async logPageContent(name) {
        const html = await this.page.content();
        let path = `./${config.catalog.content}/${name}-${this.getNowDateTimeString()}.html`;
        await fs.writeFileSync(path, html);
    }
    async logCurrentState(name) {
        try {
            await this.makeScreenshot(name);
        } catch(errorScreenshot) {
            logger.warn(`Failed to make screenshot name: "${name}" error: ${formatError(errorScreenshot)}`);
        }
        try {
            await this.logPageContent(name);
        } catch(errorContent) {
            logger.warn(`Failed to log content name: "${name}" error: ${formatError(errorContent)}`);
        }
    }
    async pageCrashedHandler(error) {
        logger.error(`Page crashed, try recover ${formatError(error)}`);
        try {
            await this.page.reload(); // soft fix
        } catch (recoveringErr) {
            // unable to reload the page, hard fix
            logger.error("Failed to reload page, " +
                "try close browser by puppeteer, " +
                formatError(error));
            try {
                await this.browser.close();
                await timeout(60 * 1000);
                await killBrowser(); // for reliability
                await timeout(30 * 1000);
            } catch (browserCloseError) { // browser close was not necessary
                logger.error('Failed to close browser, ' +
                    'try terminate process by execute "pkill chrome", ' +
                    formatError(browserCloseError));
                try {
                    await killBrowser();
                    await timeout(30 * 1000);
                } catch (killChromeError) {
                    logger.error('Failed to execute "pkill chrome", ' +
                        formatError(killChromeError));
                }
            }
            this.browser = browserLauncher();
            this.page = await this.createPage();
        }
    }
    async setRequestInterceptor(page) {
        await page.setRequestInterception(true);
        page.on('request', interceptedRequest => {
            if (interceptedRequest.url().includes('yandex.ru') || interceptedRequest.url().includes('google'))
                interceptedRequest.abort();
            else
                interceptedRequest.continue();
        });
    }
    async retry(action, count, errorHandler, deep = 0) {
        try {
            await action();
        } catch (e) {
            if (deep < count && await errorHandler(e) === true)
                await this.retry(action, count, errorHandler, deep + 1);
            else
                throw e;
        }
    }
    async retryPageHandler(action) {
        await this.retry(async () => await action(), 2, async (error) => {
            if(error.message === 'Page crashed!') {
                await this.pageCrashedHandler(error);
                return true;
            } else if (error.message.includes('Protocol error (Page.navigate): Session closed.')){
                logger.error('Lost puppeteer session with browser, ' +
                    'try terminate chrome process by execute "pkill chrome"' +
                    formatError(error));
                await killBrowser();
                await timeout(30 * 1000);
                this.browser = await browserLauncher();
                this.page = await this.createPage();
                return true;
            }
        });
    }
    async restartBrowser() {
        this.browser = await restartBrowser(this.browser);
        this.page = await this.createPage();
    }
    async createPage() {
        const page = await this.browser.newPage();
        await this.setRequestInterceptor(page);
        return page;
    }
}

module.exports = {Parser};
