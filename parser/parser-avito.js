const fs = require('fs');
const {Parser} = require('./parser');
const {AvitoItem} = require('./parse-item');
const {AvitoItemCollection} = require('./parse-item-collection');
const {ItemSelectors} = require('./item-selectors');
const ocr = require('./ocr');
const config = require('./config');
const moment = require('moment');

class ParserAvito extends Parser
{
    constructor(browser, cookies) {
        const startUrl = config.avito.startUrl;
        const selectors = new ItemSelectors(
            '.title-info-title-text',
            '.item-description-text, .item-description-html',
            '.item-view-content-right .seller-info-name',
            ".title-info-metadata-item-redesign",
            '.item-address__string',
            '[data-marker="item-phone-button/card"]',
            '.item-phone-big-number',
            '[data-marker="header/login-button"]'
        );
        super(browser, cookies, startUrl, selectors)
    }
    async start() {
        return await super.start();
    }
    async getUrlsOfItems() {
        let urls = [];
        let self = this;
        let i = 1;
        do {
            const pageUrls = await parseAvitoItemListPage();
            if(pageUrls.length === 0)
                break;
            urls = urls.concat(pageUrls);
            const nextPageEl = await this.page.$('[data-marker="pagination-button/next"]:not(.pagination-item_readonly-2V7oG)');
            if (nextPageEl === null)
                break;
            await nextPageEl.click();
            await this.page.waitFor(5000);
        } while (true);

        return urls;

        async function parseAvitoItemListPage() {
            async function parseItemHref(item) {
                const url = await (await item.$('.snippet-link')).evaluate(node => node.href);
                urls.push(url);
            }

            let itemsSelector = '.item__line';
            await self.page.waitForSelector(itemsSelector);
            const itemsList = await self.page.$$(itemsSelector);
            const urls = [];
            if(itemsList.length === 0) {
                await self.logCurrentState('not-found-avito-items');
            }
            for(const item of itemsList) {
                const date = await (await item.$('[data-marker="item-date"]')).evaluate(node => node.textContent.trim());
                let yesterday;
                if (date.indexOf('час') !== -1 || date.indexOf('мин') !== -1 || date.indexOf('Сегодня') !== -1) {
                    await parseItemHref(item);
                } else {
                    yesterday = date.match(/Вчера (\d+):\d+/);
                    if(yesterday && yesterday.length > 1 && parseInt(yesterday[1]) >= config.cron.hour) {
                        await parseItemHref(item);
                    }
                }
            }
            return urls;
        }
    }
    async parsePhoneNumber() {
        const phoneImgSelector = `${this.selectors.phoneNumberModal}  img`;
        await this.page.waitForSelector(phoneImgSelector);
        const phoneNumberEl = await this.page.$(phoneImgSelector);
        return await ocr(phoneNumberEl);
    }
    parseRegionEl(text) {
        let region, city;
        if(text && text.split(', ').length > 1) {
            [region, city] = text.split(', ');
            if (region === 'Москва' || region === 'Санкт-Петербург') {
                city = region;
                region = undefined;
            }
        } else region = text;
        return {region, city};
    }

    createParseItem(title, description, name, date, region, city, phone, email, url) {
        return new AvitoItem(title, description, name, date, region, city, phone, email, url);
    }

    createParseItemCollection() {
        return new AvitoItemCollection()
    }
}

module.exports = {ParserAvito};
