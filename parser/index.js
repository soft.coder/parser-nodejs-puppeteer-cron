const process = require('process');
const moment = require('moment');
const config = require('./config');
const logger = require('./logger');
const fs = require('fs');
const {ParserAvito} = require('./parser-avito');
const {ParserYoula} = require('./parser-youla');
const {RequiredAuthorization} = require('./errors');
const {Mailer} = require('./mailer');
const { ProcessManager } = require('./process-manager');
const { Request, Aes } = require('parser-avito-and-youla-common');
const {AvitoItemCollection, YoulaItemCollection} = require('./parse-item-collection');
const {formatError, browserLauncher, restartBrowser, killBrowser, cleanCatalog} = require('./utils');


process.on('unhandledRejection', error => {
    logger.error(`unhandledRejection: ${formatError(error)}`);
});

async function main() {

    let browser = await browserLauncher();

    let cookies;
    try {
        const userData = fs.readFileSync(config.userDataPath);
        // fs.unlink(config.userDataPath);
        cookies = JSON.parse(userData);
    } catch (error) {
        logger.error(`Unexpected error occurred when read user data, ${formatError(error)}`);
    }
    try {
        cleanCatalog(config.catalog.screenshot);
    } catch(e) { }
    try {
        cleanCatalog(config.catalog.content);
    } catch(e) { }
    let avitoItems, youlaItems;
    try {
        const parserYoula = new ParserYoula(browser, cookies);
        youlaItems = await parserYoula.start();
        logger.info(`Parsed ${youlaItems.getLength()} youla items: ${youlaItems.toJson()}`);
        await sendCsv(youlaItems);
        browser = await restartBrowser(browser);
        const parserAvito = new ParserAvito(browser, cookies);
        avitoItems = await parserAvito.start();
        logger.info(`Parsed ${avitoItems.getLength()} avito items: ${avitoItems.toJson()}`);
        await sendCsv(avitoItems);
    } catch(error) {
        if(error instanceof RequiredAuthorization) {
            await StartServerAndSendEmail();
        } else {
            logger.error(`Unexpected error occurred when parse and send parsed data ${formatError(error)}`);
            throw error;
        }
    } finally {
        try {
            await SendEmailAboutCountOfParsedAdvertisement(avitoItems, youlaItems);
        } catch (e) { }
        try {
            await browser.close();
        } catch(e) {}
        await killBrowser();
    }

    async function sendCsv(items) {
        if(items.getLength() > 0) {
            const csv = items.toCsv();
            const buffer = Buffer.from(csv, 'utf-8');
            const base64 = buffer.toString('base64');
            const request = new Request('parser.aleksanp.beget.tech');
            const type = items instanceof AvitoItemCollection ? 'avito' : 'youla';
            await request.send(`/receive-parsed-data.php?password=6407669d23c48c04e988f356b85bb8e6&data=${type}`,
                base64, 'POST');
        }
    }

    async function StartServerAndSendEmail() {
        try {
            ProcessManager.runServer();
            await Mailer.sendRequiredManualAuthorization();
        } catch (e) {
            logger.error('Error occurred whe try start server that ' +
                'receive user data and send email ' +
                `about required manual auth ${formatError()}`);
        }

    }
    async function SendEmailAboutCountOfParsedAdvertisement(avitoParsedItems, youlaParsedItems) {
        const getCount = (items) => items ? items.getLength() : 'ошибка';
        try {
            const avitoCount = getCount(avitoParsedItems);
            const youlaCount = getCount(youlaParsedItems);
            await Mailer.sendCountParsedItems(avitoCount, youlaCount);
        } catch (e) {
            logger.error('Error occurred whe try send email about count of parsed advertisement' +
                'receive user data and send email ' +
                `about required manual auth ${formatError()}`);
        }
    }
}

module.exports = {main};
