const winston = require('winston');
const { format } = winston;
const { combine, timestamp, json } = format;
require('winston-daily-rotate-file');

const transport = new (winston.transports.DailyRotateFile)({
    filename: 'application-%DATE%.log',
    datePattern: 'YYYY-MM-DD',
    zippedArchive: true,
    dirname: 'logs',
    maxSize: '20m',
    maxFiles: '14d'
});


module.exports = winston.createLogger({
    format: combine(timestamp(), json()),
    transports: [
        new winston.transports.Console(),
        transport
    ]
});
