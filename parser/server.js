const process = require('process');
const http = require('http');
const logger = require('./logger');
const config = require('./config');
const {Rsa, Aes} = require('parser-avito-and-youla-common');
const {formatError} = require('./utils');

process.on('unhandledRejection', error => {
    logger.error(`unhandledRejection: ${formatError(error)}`);
    process.exit();
});

class Server
{
    constructor(port) {
        this.hostname = config.hostname;
        this.port = port;
        this.aes = new Aes();
    }
    listenRequestData(req, path, method, callback) {
        if(req.method === method && req.url === path) {
            let body = [];
            req.on('data', (chunk) => {
                body.push(chunk);
            }).on('end', () => {
                body = Buffer.concat(body).toString();
                callback(body);
            });
        }
    }
    run(path, callback, onComplete) {
        const server = http.createServer((req, res) => {
            this.listenRequestData(req, path, 'POST', (body) => {
                try {
                    body = this.aes.decrypt(body);
                    callback(body);
                    Server.responseCode(res, 200, "OK");
                    if(onComplete) onComplete();
                } catch(error) {
                    logger.error("Error occurred when decrypt, parse and save user data, error: " + formatError(error));
                    Server.responseCode(res, 400, "Bad request");
                }
            });
            this.listenRequestData(req, path, 'PUT', (body) => {
                try {
                    body = Rsa.decrypt(body);
                    this.aes.importKeyAndIv(body);
                    Server.responseCode(res, 200, "OK");
                } catch (error) {
                    logger.error("Error occurred when try decrypt and import aes key and iv error: " + formatError(error));
                    Server.responseCode(res, 400, "Bad request");
                }
            });
            if(!(req.url === path && (req.method === 'PUT' || req.method === 'POST')))
                Server.responseCode(res, 404, "Not found");
        });

        server.listen(this.port, this.hostname, () => {
            logger.info(`Server running at http://${this.hostname}:${this.port}/`);
        });


    }
    static responseCode(res, code, message) {
        res.statusCode = code;
        res.statusMessage = message;
        res.end();
    }
}

module.exports = {Server};
