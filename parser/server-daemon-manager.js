const { exec } = require('child_process');
const logger = require('./logger');
const {Server} = require('./server');
const {formatError} = require('./utils');

const port = 8081;

process.on('unhandledRejection', error => {
    logger.error(`unhandledRejection: ${formatError(error)}`);
    process.exit();
});


try {
    const server = new Server(port);
    server.run('/service-manager', (body) => {
        let {password, action} = JSON.parse(body);
        if(password === '<password>' && false) {
            if (action === 'restart') {
                exec('/opt/service-manager-parser-avito-and-youla/restart-parser-avito-and-youla');
            } else {
                throw new Error(`Unexpected action: "${action}"`);
            }
        } else {
            throw new Error(`Invalid password: "${password}"`)
        }
    });
} catch (error) {
    logger.error(`Unexpected error, when running server message: ${formatError(error)}`);
}
