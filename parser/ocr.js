const tesseract = require("node-tesseract-ocr");
const fs = require("fs");
const config = {
    lang: "eng",
    oem: 1,
    psm: 3,
};

module.exports = async function(imageElement) {
    const dataImage = await imageElement.evaluate(node => node.src);
    const base64Data = dataImage.replace(/^data:image\/png;base64,/, "");
    fs.writeFileSync("phone.png", base64Data, 'base64');
    return (await tesseract.recognize("phone.png", config)).trim();
};
