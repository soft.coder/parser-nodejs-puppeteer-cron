const nodemailer = require("nodemailer");
const logger = require('./logger');

class Mailer
{
    static async send(subject, text) {
        let transporter = nodemailer.createTransport({
            host: "<host>",
            port: 465,
            secure: true, // true for 465, false for other ports
            auth: {
                user: "<username>", // generated ethereal user
                pass: "<password>" // generated ethereal password
            }
        });

        let info = await transporter.sendMail({
            from: '"Парсер" <email>', // sender address
            to: "<email>", // list of receivers
            subject: subject, // Subject line
            text: text, // plain text body
        });
    }
    static async sendRequiredManualAuthorization() {
        try {
            let subject = "Парсер запрашивает ручную авторизации!";
            let text = "Для того чтобы парсер заработал, вам нужно через программу авторизации вручную пройти каптчу на сайте авито и ввести смс код подтверждения на сайте юла!";
            await Mailer.send(subject, text);
            logger.info("Email about required authorization through nodejs app was successfully sent ");
        } catch(error) {
            logger.error("Error occurred when try sending email about required authorization, message: " + error.message + " stack: " + error.stack);
        }
    }
    static async sendCountParsedItems(avitoCount, youlaCount) {
        try {
            let subject = "Информация о количестве спарсенных объявлений";
            let text = `Авито: ${avitoCount}, Юла:${youlaCount}!`;
            await Mailer.send(subject, text);
            logger.info("Email about parsed count of advertisement through nodejs app was successfully sent ");
        } catch(error) {
            logger.error("Error occurred when try sending email about parsed count of advertisement, message: " + error.message + " stack: " + error.stack);
        }
    }
}

module.exports = { Mailer };
