class NotImplementedAbstractMethodError extends Error {
}
class RequiredAuthorization extends Error{
}

module.exports = {NotImplementedAbstractMethodError, RequiredAuthorization};
