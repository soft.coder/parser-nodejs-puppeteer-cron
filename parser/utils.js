const logger = require('./logger');
const puppeteer = require('puppeteer-extra');
const { exec } = require('child_process');
const process = require('process');
const StealthPlugin = require('puppeteer-extra-plugin-stealth');
const config = require('./config');
puppeteer.use(StealthPlugin());

async function browserLauncher() {
    let options = {
        headless: config.browser.headless,
        defaultViewport: {
            width: 1920,
            height: 1080
        },
        slowMo: 100,
        userDataDir: './chromeUserDataDir',
        args: ['--no-sandbox', '--disable-setuid-sandbox']};
    if(process.platform !== 'win32' && process.platform !== 'win64') {
        options["args"] = ['--no-sandbox', '--disable-setuid-sandbox'];
    }
    return await puppeteer.launch(options);
}
async function killBrowser() {
    let command;
    try {
        if(process.platform === 'win32' || process.platform === 'win64') {
            command = 'taskkill /f /im chrome.exe';
        } else {
            command = 'pkill chrome';
        }
        await exec(command);
    } catch (execError) {
        logger.error(`Failed to execute "${command}" ` +
            `on os platform "${process.platform}", ` +
            formatError(execError));
        throw execError;
    }
}

async function cleanCatalog(catalogName) {
    let commandRemove, commandMakeDir;
    if (process.platform === 'win32' || process.platform === 'win64') {
        commandRemove = `RMDIR /S /Q ${catalogName}`;
        commandMakeDir = `MKDIR ${catalogName}`
    } else {
        commandRemove = `rm -R ${catalogName}`;
        commandMakeDir = `mkdir ${catalogName}`;
    }
    try {
        await exec(commandRemove);
        await exec(commandMakeDir);
    } catch(execError) {
        logger.error(`Failed to clean catalog: "${catalogName}" error: ${formatError(execError)}`);
        throw execError;
    }
}

async function restartBrowser(browser) {
    try {
        await browser.close();
    } catch (e) {
        logger.warn("Failed to close browser, " + formatError(e));
    }
    await timeout(60 * 1000);
    await killBrowser(); // for reliability
    await timeout(30 * 1000);
    return await browserLauncher();
}



async function timeout(milliseconds) {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve();
        }, milliseconds);
    });
}

function formatError(error) {
    return `constructor: '${error && error.constructor}',` +
        `message: '${error && error.message}', ` +
        `fileName: '${error && error.fileName}', ` +
        `lineNumber: '${error && error.lineNumber}'` +
        `stack: '${error && error.stack}'`

}

module.exports = {formatError, browserLauncher, killBrowser, restartBrowser, timeout, cleanCatalog};
