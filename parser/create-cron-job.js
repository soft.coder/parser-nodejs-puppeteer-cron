const CronJob = require('cron').CronJob;
const process = require('process');
const {main} = require('./index');
const logger = require('./logger');
const config = require('./config');
const {formatError} = require('./utils');

let id = 0;
const job = new CronJob(`0 0 ${config.cron.hour} * * *`, function() {
    id = id < Number.MAX_SAFE_INTEGER ? id++ : 1;
    logger.info(`start cron job id: ${id}`);
    main()
        .then(() => {
            logger.info(`cron job id:${id} successfully completed`);
        })
        .catch(error => {
            logger.info(`cron job id:${id} throw error ${formatError(error)}`);
            process.exitCode = 1;
            job.stop();
        });
}, null, true);
