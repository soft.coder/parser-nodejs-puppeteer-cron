
class ParseItem
{
    constructor(title, description, name, date, region, city, phone, email, url) {
        this.title = title;
        this.description = description;
        this.name = name;
        this.date = date;
        this.region = region;
        this.city = city;
        this.phone = phone;
        this.email = email;
        this.url = url;
    }
    toObject() {
        let result = {};
        for(let propName in this) {
            if(this.hasOwnProperty(propName)) {
                result[propName] = this[propName];
            }
        }
        return result;
    }
}
class AvitoItem extends  ParseItem
{
    constructor(title, description, name, date, region, city, phone, email, url) {
        super(title, description, name, date, region, city, phone, email, url);
    }
}
class YoulaItem extends  ParseItem
{
    constructor(title, description, name, date, region, city, phone, email, url) {
        super(title, description, name, date, region, city, phone, email, url);
    }
}

module.exports = {ParseItem, AvitoItem, YoulaItem};
